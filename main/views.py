from django.contrib.auth import logout
from django.shortcuts import render, redirect
from django.views import View
from django.views.generic import ListView, CreateView

from .form import BookForm, SupplyForm, SaleForm
from .models import Book, Supply, Sale


class BookListView(ListView):
    template_name = 'books.html'
    model = Book


class CreateBooksView(CreateView):
    template_name = 'create.html'
    form_class = BookForm

    def post(self, request, *args, **kwargs):
        form = BookForm(request.POST)

        if form.is_valid():
            response = form.save(commit=False)
            response.user = request.user
            response.save()
            return redirect('books')
        return render(request, 'create.html', context={'form': form})


class BookEdit(View):
    def get(self, request, id):
        book = Book.objects.get(id=id)
        form = BookForm(instance=book)
        return render(request, 'edit.html', context={'form': form, 'book': book})

    def post(self, request, id):
        book = Book.objects.get(id=id)
        form = BookForm(request.POST, instance=book)
        if form.is_valid():
            form.save()
            return redirect('books')
        return render(request, 'edit.html', context={'form': form, 'book': book})


class SupplyListView(ListView):
    template_name = 'supply.html'
    model = Book

    def get_queryset(self):
        return Book.objects.filter()


class CreateSupplyView(CreateView):
    template_name = 'create_supply.html'
    form_class = SupplyForm

    def post(self, request, *args, **kwargs):
        form = SupplyForm(request.POST)

        if form.is_valid():
            book_id = int(form.cleaned_data['book_name'])
            date = form.cleaned_data['date']
            count_books = int(form.cleaned_data['count_books'])

            supply = Supply.objects.create(date=date, count=count_books)
            book = Book.objects.get(id=book_id)
            book.supply.add(supply)
            book.count += count_books
            book.save()
            return redirect('supply')

        return render(request, 'create_supply.html', context={'form': form})


class SalesListView(ListView):
    template_name = 'sales.html'
    model = Sale


class CreateSalesView(CreateView):
    template_name = 'create_sales.html'
    form_class = SaleForm

    def post(self, request, *args, **kwargs):
        form = SaleForm(request.POST)

        if form.is_valid():
            client = form.cleaned_data['client']
            employee = form.cleaned_data['employee']
            books = form.cleaned_data['books']

            cost = sum(books.values_list('price', flat=True))

            sale = Sale.objects.create(
                client=client,
                employee=employee,
                cost=cost
            )
            sale.book.add(*books)
            return redirect('sales')
        return render(request, 'create_sales.html', context={'form': form})


def logout_view(request):
    logout(request)
    return redirect('books')


def account(request):
    return render(request, 'account.html', context={'user': request.user})