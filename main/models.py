from django.db import models


class Supply(models.Model):
    date = models.DateField(
        verbose_name='Дата поставки'
    )
    count = models.IntegerField(
        verbose_name='Количество поставленных книг',
        blank=True, null=True
    )

    def __str__(self):
        return self.date


class Book(models.Model):
    name = models.CharField(
        verbose_name='Наименование',
        max_length=1024,
    )
    author = models.CharField(
        verbose_name='Автор',
        max_length=1024
    )
    publishing = models.CharField(
        verbose_name='Издательство',
        max_length=1024
    )
    year_publication = models.IntegerField(
        verbose_name='Год издания'
    )
    count = models.IntegerField(
        verbose_name='Количество экзмепляров'
    )
    price = models.FloatField(
        verbose_name='Цена'
    )
    purchase_price = models.FloatField(
        verbose_name='Закупочкая цена'
    )

    supply = models.ManyToManyField(
        to=Supply,
        related_name='supplys',
        verbose_name='Поставка',
        blank=True, null=True
    )

    def __str__(self):
        return self.name


class Sale(models.Model):
    client = models.CharField(
        verbose_name='Клиент',
        max_length=1024,
    )
    employee = models.CharField(
        verbose_name='Сотрудник',
        max_length=1024,
    )
    book = models.ManyToManyField(
        verbose_name='Книги',
        to='main.Book',
    )
    cost = models.FloatField(
        verbose_name='Сумма',
    )

    def __str__(self):
        return self.cost

    @property
    def get_books(self):
        books = [' - '.join((book.name, str(book.price) + ' рублей')) for book in self.book.all()]
        return books