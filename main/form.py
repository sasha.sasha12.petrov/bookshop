from django import forms

from .models import Book, Supply, Sale


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = [
            'name',
            'author',
            'publishing',
            'year_publication',
            'count',
            'price',
            'purchase_price',
        ]


class SupplyForm(forms.ModelForm):
    book_name = forms.ChoiceField(
        label='Наименование книги',
        choices=(),
        required=True,
        widget=forms.Select(attrs={'class': 'form-select'}),
    )
    count_books = forms.IntegerField(
        label='Количество книг в поставке'
    )
    date = forms.DateField(
        label='Дата поставки'
    )

    class Meta:
        model = Book
        fields = [
            'book_name',
            'count_books',
            'date'
        ]

    def __init__(self, *args, **kwargs):
        super(SupplyForm, self).__init__(*args, **kwargs)
        self.fields['book_name'].choices = ((book.id, book.name) for book in Book.objects.all())


class SaleForm(forms.ModelForm):
    books = forms.ModelMultipleChoiceField(
        label='Книги',
        queryset=Book.objects.all(),
        widget=forms.SelectMultiple(attrs={'class': 'form-select'}),
    )

    class Meta:
        model = Sale
        fields = [
            'client',
            'employee',
            'books',
        ]