from django.urls import path
from . import views


urlpatterns = [
    path('', views.BookListView.as_view(), name='books'),
    path('books/create', views.CreateBooksView.as_view(), name='create'),
    path('books/<id>/edit/', views.BookEdit.as_view(), name='edit'),
    path('supply', views.SupplyListView.as_view(), name='supply'),
    path('sales', views.SalesListView.as_view(), name='sales'),
    path('sales/create', views.CreateSalesView.as_view(), name='create_sales'),
    path('supply/create_supply', views.CreateSupplyView.as_view(), name='create_supply'),
    path('accounts/logout', views.logout_view, name='loggetout'),
    path('account', views.account, name='account'),
]


